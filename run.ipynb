{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# `gsmo` apt/pip example\n",
    "This module takes a `limit` parameter and generates a Parquet file and couple plots showing various numeric properties of the natural numbers up to that `limit`.\n",
    "\n",
    "It demonstrates using [`gsmo`](https://github.com/runsascoded/gsmo) to:\n",
    "1. easily configure a Docker image with specific `apt` and `pip` library dependencies (see [`gsmo.yml`](./gsmo.yml))\n",
    "2. run a notebook (mounted in a Docker container from 1., and taking on-the-fly arguments from the command line\n",
    "3. git-commit the results inside the container (in a manner that is also accessible/consistent outside the container)\n",
    "\n",
    "## run configs (via papermill)\n",
    "The first cell below has the metadata tag `parameters`, allowing this module to illustrate passing \"run configs\" to a `gsmo` module (using [`papermill`](https://papermill.readthedocs.io/en/latest/)).\n",
    "\n",
    "Override configs w/ YAML string on the CLI:\n",
    "```bash\n",
    "gsmo -y 'limit: 50' example/factors\n",
    "```\n",
    "\n",
    "Override configs w/ path to a YAML file:\n",
    "```bash\n",
    "echo 'limit: 50' > run.yml\n",
    "gsmo -Y run.yml example/factors\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Parameters"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "parameters"
    ]
   },
   "outputs": [],
   "source": [
    "out = 'ints.parquet'  # output parquet path for computed DataFrame\n",
    "plot = 'primes.png'  # output plot path\n",
    "graph = 'graph.png'  # output dot graph path\n",
    "limit = 20          # integer to analyze up to (inclusive)\n",
    "interactive = False  # render image in notebook as plotly widget vs. static image"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Imports / Utilities"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from utz import *"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def factors(n, ret=None, f=2, M=None):\n",
    "    '''Return a dict mapping prime factors of `n` to their multiplicities\n",
    "    \n",
    "    Notes:\n",
    "    - only `n` should be passed on the outer call\n",
    "    - computation proceeds inductively starting from a factor `f` of 2\n",
    "    - `ret` is mutated by recursive calls and ultimately returned\n",
    "    - `M` is the largest possible remaining factor\n",
    "    '''\n",
    "    # Base cases / default values\n",
    "    ret = ret or {}\n",
    "    if n == 1: return ret\n",
    "    if n <= 0: raise ValueError(n)\n",
    "    M = M or int(floor(sqrt(n)))\n",
    "    if f > M:\n",
    "        ret[n] = 1\n",
    "        return ret\n",
    "\n",
    "    # Remove all factors of `f` in `n`\n",
    "    p = 0\n",
    "    while n % f == 0:\n",
    "        n //= f\n",
    "        p += 1\n",
    "    if p:\n",
    "        ret[f] = p\n",
    "        M = min(M,n)\n",
    "\n",
    "    # Recurse with what's left of `n`, an incremented factor `f`, and an (possibly decreased) maximum factor `M`\n",
    "    return factors(n, ret, f+1, M)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Build DataFrame of natural numbers' prime factors"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def make_row(n):\n",
    "    facts = factors(n)\n",
    "    facts['n'] = n\n",
    "    return facts\n",
    "\n",
    "prime_factor_grid = \\\n",
    "    DF([\n",
    "        make_row(n)\n",
    "        for n in range(1, limit+1)\n",
    "    ]) \\\n",
    "    .set_index('n') \\\n",
    "    .fillna(0) \\\n",
    "    .applymap(int)\n",
    "prime_factor_grid.columns.name = 'prime'\n",
    "prime_factor_grid"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Melt DataFrame into (factee,factor,power) triples"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "prime_factors = \\\n",
    "    prime_factor_grid \\\n",
    "    .reset_index() \\\n",
    "    .melt(id_vars=['n']) \\\n",
    "    .rename(columns={'value':'power'}) \\\n",
    "    .set_index(['n','prime']) \\\n",
    "    .sort_index()\n",
    "prime_factors = prime_factors[prime_factors.power > 0]\n",
    "prime_factors"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Plot numbers + prime factors"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import plotly.express as px\n",
    "fig = px.scatter(prime_factors.reset_index(), x=\"n\", y=\"prime\", size='power', color='power',)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "if interactive:\n",
    "    img = fig.show()\n",
    "else:\n",
    "    img_bytes = fig.to_image(format=\"png\")\n",
    "    from IPython.display import Image\n",
    "    img = Image(img_bytes)\n",
    "img"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig.write_image(plot)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Identify primes"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "is_prime = prime_factor_grid.apply(lambda r: r.sum() == 1, axis=1)\n",
    "is_prime"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Write Parquet output file\n",
    "Concatenate the two DFs above side-by-side (`sxs`), and write a `.parquet`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "is_prime.name = 'is_prime'\n",
    "df = sxs(is_prime, prime_factor_grid)\n",
    "df.columns = df.columns.astype(str)\n",
    "df"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df.to_parquet(out)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Generate divisor graph\n",
    "Uses `graphviz`, which is a required `apt` dependency in the module's [`gsmo.yml`](gsmo.yml) config:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def make_divisors(df):\n",
    "    [n] = df.n.unique()\n",
    "    prime_divisors = df[['prime','power']].values.tolist()\n",
    "    divisors = set([1])\n",
    "    while prime_divisors:\n",
    "        prime, power = prime_divisors.pop(0)\n",
    "        base = 1\n",
    "        prev = divisors.copy()\n",
    "        for degree in range(1, power+1):\n",
    "            base *= prime\n",
    "            for divisor in prev:\n",
    "                divisors.add(prime*divisor)\n",
    "    divisors = sorted(divisors)\n",
    "    ret = DF(divisors, columns=['divisor'])\n",
    "    return ret.set_index('divisor')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "prime_factor_triples = prime_factors.reset_index()\n",
    "divisors = prime_factor_triples.groupby('n').apply(make_divisors).reset_index()\n",
    "divisors"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "len(prime_factor_grid.columns)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "with open('colors.txt','r') as f:\n",
    "    colors = [ line.strip() for line in f.readlines() ]\n",
    "\n",
    "prime_colors = { prime: colors[i%len(colors)] for i,prime in enumerate(prime_factor_grid.columns) }\n",
    "prime_colors"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from graphviz import Digraph\n",
    "\n",
    "dot = Digraph(comment='Divisors')\n",
    "node = dot.node\n",
    "edge = dot.edge\n",
    "\n",
    "for n, prime, power in prime_factor_triples.values:\n",
    "    base = n // prime\n",
    "    edge(str(base), str(n), color=prime_colors[prime])\n",
    "\n",
    "dot"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "name, xtn = splitext(graph)\n",
    "assert xtn.startswith('.')\n",
    "xtn = xtn[1:]\n",
    "dot.render(filename=name, format=xtn)\n",
    "remove(name)  # graphviz also writes the graph in DOT format to an extension-less file, for some reason"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Exit w/ success msg"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "num_primes = is_prime.sum(); num_primes"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from gsmo import OK\n",
    "OK(f'Found {num_primes} primes ∈ [1,{limit})')"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}