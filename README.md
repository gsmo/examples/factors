# `gsmo` apt/pip example
This module takes a `limit` parameter and generates a Parquet file and couple plots showing various numeric properties of the natural numbers up to that `limit`.

It demonstrates using [`gsmo`](https://github.com/runsascoded/gsmo) to:
1. easily configure a Docker image with specific `apt` and `pip` library dependencies (see [`gsmo.yml`](./gsmo.yml))
2. run a notebook (mounted in a Docker container from 1., and taking on-the-fly arguments from the command line
3. git-commit the results inside the container (in a manner that is also accessible/consistent outside the container)

## run configs (via papermill)
The first cell below has the metadata tag `parameters`, allowing this module to illustrate passing "run configs" to a `gsmo` module (using [`papermill`](https://papermill.readthedocs.io/en/latest/)).

Override configs w/ YAML string on the CLI:
```bash
gsmo -Y 'limit: 50' example/factors
```

Override configs w/ path to a YAML file:
```bash
echo 'limit: 50' > run.yml
gsmo -y run.yml example/factors
```

## Parameters


```python
out = 'ints.parquet'  # output parquet path for computed DataFrame
plot = 'primes.png'  # output plot path
graph = 'graph.png'  # output dot graph path
limit = 20          # integer to analyze up to (inclusive)
interactive = False  # render image in notebook as plotly widget vs. static image
```

## Imports / Utilities


```python
from utz import *
import json
from math import sqrt
```


```python
def factors(n, ret=None, f=2, M=None):
    '''Return a dict mapping prime factors of `n` to their multiplicities
    
    Notes:
    - only `n` should be passed on the outer call
    - computation proceeds inductively starting from a factor `f` of 2
    - `ret` is mutated by recursive calls and ultimately returned
    - `M` is the largest possible remaining factor
    '''
    # Base cases / default values
    ret = ret or {}
    if n == 1: return ret
    if n <= 0: raise ValueError(n)
    M = M or int(floor(sqrt(n)))
    if f > M:
        ret[n] = 1
        return ret

    # Remove all factors of `f` in `n`
    p = 0
    while n % f == 0:
        n //= f
        p += 1
    if p:
        ret[f] = p
        M = min(M,n)

    # Recurse with what's left of `n`, an incremented factor `f`, and an (possibly decreased) maximum factor `M`
    return factors(n, ret, f+1, M)
```

## Build DataFrame of natural numbers' prime factors


```python
def make_row(n):
    facts = factors(n)
    facts['n'] = n
    return facts

prime_factor_grid = \
    DF([
        make_row(n)
        for n in range(1, limit+1)
    ]) \
    .set_index('n') \
    .fillna(0) \
    .applymap(int)
prime_factor_grid.columns.name = 'prime'
prime_factor_grid
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th>prime</th>
      <th>2</th>
      <th>3</th>
      <th>5</th>
      <th>7</th>
      <th>11</th>
      <th>13</th>
      <th>17</th>
      <th>19</th>
    </tr>
    <tr>
      <th>n</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>1</th>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>2</th>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>3</th>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>4</th>
      <td>2</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>5</th>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>6</th>
      <td>1</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>7</th>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>8</th>
      <td>3</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>9</th>
      <td>0</td>
      <td>2</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>10</th>
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>11</th>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>12</th>
      <td>2</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>13</th>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>14</th>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>15</th>
      <td>0</td>
      <td>1</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>16</th>
      <td>4</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>17</th>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <th>18</th>
      <td>1</td>
      <td>2</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>19</th>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
    </tr>
    <tr>
      <th>20</th>
      <td>2</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
  </tbody>
</table>
</div>



## Melt DataFrame into (factee,factor,power) triples


```python
prime_factors = \
    prime_factor_grid \
    .reset_index() \
    .melt(id_vars=['n']) \
    .rename(columns={'value':'power'}) \
    .set_index(['n','prime']) \
    .sort_index()
prime_factors = prime_factors[prime_factors.power > 0]
prime_factors
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th></th>
      <th>power</th>
    </tr>
    <tr>
      <th>n</th>
      <th>prime</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>2</th>
      <th>2</th>
      <td>1</td>
    </tr>
    <tr>
      <th>3</th>
      <th>3</th>
      <td>1</td>
    </tr>
    <tr>
      <th>4</th>
      <th>2</th>
      <td>2</td>
    </tr>
    <tr>
      <th>5</th>
      <th>5</th>
      <td>1</td>
    </tr>
    <tr>
      <th rowspan="2" valign="top">6</th>
      <th>2</th>
      <td>1</td>
    </tr>
    <tr>
      <th>3</th>
      <td>1</td>
    </tr>
    <tr>
      <th>7</th>
      <th>7</th>
      <td>1</td>
    </tr>
    <tr>
      <th>8</th>
      <th>2</th>
      <td>3</td>
    </tr>
    <tr>
      <th>9</th>
      <th>3</th>
      <td>2</td>
    </tr>
    <tr>
      <th rowspan="2" valign="top">10</th>
      <th>2</th>
      <td>1</td>
    </tr>
    <tr>
      <th>5</th>
      <td>1</td>
    </tr>
    <tr>
      <th>11</th>
      <th>11</th>
      <td>1</td>
    </tr>
    <tr>
      <th rowspan="2" valign="top">12</th>
      <th>2</th>
      <td>2</td>
    </tr>
    <tr>
      <th>3</th>
      <td>1</td>
    </tr>
    <tr>
      <th>13</th>
      <th>13</th>
      <td>1</td>
    </tr>
    <tr>
      <th rowspan="2" valign="top">14</th>
      <th>2</th>
      <td>1</td>
    </tr>
    <tr>
      <th>7</th>
      <td>1</td>
    </tr>
    <tr>
      <th rowspan="2" valign="top">15</th>
      <th>3</th>
      <td>1</td>
    </tr>
    <tr>
      <th>5</th>
      <td>1</td>
    </tr>
    <tr>
      <th>16</th>
      <th>2</th>
      <td>4</td>
    </tr>
    <tr>
      <th>17</th>
      <th>17</th>
      <td>1</td>
    </tr>
    <tr>
      <th rowspan="2" valign="top">18</th>
      <th>2</th>
      <td>1</td>
    </tr>
    <tr>
      <th>3</th>
      <td>2</td>
    </tr>
    <tr>
      <th>19</th>
      <th>19</th>
      <td>1</td>
    </tr>
    <tr>
      <th rowspan="2" valign="top">20</th>
      <th>2</th>
      <td>2</td>
    </tr>
    <tr>
      <th>5</th>
      <td>1</td>
    </tr>
  </tbody>
</table>
</div>



## Plot numbers + prime factors


```python
import plotly.express as px
fig = px.scatter(prime_factors.reset_index(), x="n", y="prime", size='power', color='power',)
```


```python
if interactive:
    img = fig.show()
else:
    img_bytes = fig.to_image(format="png")
    from IPython.display import Image
    img = Image(img_bytes)
img
```




    
![png](README_files/README_12_0.png)
    




```python
fig.write_image(plot)
```

## Identify primes


```python
is_prime = prime_factor_grid.apply(lambda r: r.sum() == 1, axis=1)
is_prime
```




    n
    1     False
    2      True
    3      True
    4     False
    5      True
    6     False
    7      True
    8     False
    9     False
    10    False
    11     True
    12    False
    13     True
    14    False
    15    False
    16    False
    17     True
    18    False
    19     True
    20    False
    dtype: bool



## Write Parquet output file
Concatenate the two DFs above side-by-side (`sxs`), and write a `.parquet`:


```python
is_prime.name = 'is_prime'
df = sxs(is_prime, prime_factor_grid)
df.columns = df.columns.astype(str)
df
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>is_prime</th>
      <th>2</th>
      <th>3</th>
      <th>5</th>
      <th>7</th>
      <th>11</th>
      <th>13</th>
      <th>17</th>
      <th>19</th>
    </tr>
    <tr>
      <th>n</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>1</th>
      <td>False</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>2</th>
      <td>True</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>3</th>
      <td>True</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>4</th>
      <td>False</td>
      <td>2</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>5</th>
      <td>True</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>6</th>
      <td>False</td>
      <td>1</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>7</th>
      <td>True</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>8</th>
      <td>False</td>
      <td>3</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>9</th>
      <td>False</td>
      <td>0</td>
      <td>2</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>10</th>
      <td>False</td>
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>11</th>
      <td>True</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>12</th>
      <td>False</td>
      <td>2</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>13</th>
      <td>True</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>14</th>
      <td>False</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>15</th>
      <td>False</td>
      <td>0</td>
      <td>1</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>16</th>
      <td>False</td>
      <td>4</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>17</th>
      <td>True</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <th>18</th>
      <td>False</td>
      <td>1</td>
      <td>2</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>19</th>
      <td>True</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
    </tr>
    <tr>
      <th>20</th>
      <td>False</td>
      <td>2</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
  </tbody>
</table>
</div>




```python
df.to_parquet(out)
```

## Generate divisor graph
Uses `graphviz`, which is a required `apt` dependency in the module's [`gsmo.yml`](gsmo.yml) config:


```python
def make_divisors(df):
    [n] = df.n.unique()
    prime_divisors = df[['prime','power']].values.tolist()
    divisors = set([1])
    while prime_divisors:
        prime, power = prime_divisors.pop(0)
        base = 1
        prev = divisors.copy()
        for degree in range(1, power+1):
            base *= prime
            for divisor in prev:
                divisors.add(prime*divisor)
    divisors = sorted(divisors)
    ret = DF(divisors, columns=['divisor'])
    return ret.set_index('divisor')
```


```python
prime_factor_triples = prime_factors.reset_index()
divisors = prime_factor_triples.groupby('n').apply(make_divisors).reset_index()
divisors
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>n</th>
      <th>divisor</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>2</td>
      <td>1</td>
    </tr>
    <tr>
      <th>1</th>
      <td>2</td>
      <td>2</td>
    </tr>
    <tr>
      <th>2</th>
      <td>3</td>
      <td>1</td>
    </tr>
    <tr>
      <th>3</th>
      <td>3</td>
      <td>3</td>
    </tr>
    <tr>
      <th>4</th>
      <td>4</td>
      <td>1</td>
    </tr>
    <tr>
      <th>5</th>
      <td>4</td>
      <td>2</td>
    </tr>
    <tr>
      <th>6</th>
      <td>5</td>
      <td>1</td>
    </tr>
    <tr>
      <th>7</th>
      <td>5</td>
      <td>5</td>
    </tr>
    <tr>
      <th>8</th>
      <td>6</td>
      <td>1</td>
    </tr>
    <tr>
      <th>9</th>
      <td>6</td>
      <td>2</td>
    </tr>
    <tr>
      <th>10</th>
      <td>6</td>
      <td>3</td>
    </tr>
    <tr>
      <th>11</th>
      <td>6</td>
      <td>6</td>
    </tr>
    <tr>
      <th>12</th>
      <td>7</td>
      <td>1</td>
    </tr>
    <tr>
      <th>13</th>
      <td>7</td>
      <td>7</td>
    </tr>
    <tr>
      <th>14</th>
      <td>8</td>
      <td>1</td>
    </tr>
    <tr>
      <th>15</th>
      <td>8</td>
      <td>2</td>
    </tr>
    <tr>
      <th>16</th>
      <td>9</td>
      <td>1</td>
    </tr>
    <tr>
      <th>17</th>
      <td>9</td>
      <td>3</td>
    </tr>
    <tr>
      <th>18</th>
      <td>10</td>
      <td>1</td>
    </tr>
    <tr>
      <th>19</th>
      <td>10</td>
      <td>2</td>
    </tr>
    <tr>
      <th>20</th>
      <td>10</td>
      <td>5</td>
    </tr>
    <tr>
      <th>21</th>
      <td>10</td>
      <td>10</td>
    </tr>
    <tr>
      <th>22</th>
      <td>11</td>
      <td>1</td>
    </tr>
    <tr>
      <th>23</th>
      <td>11</td>
      <td>11</td>
    </tr>
    <tr>
      <th>24</th>
      <td>12</td>
      <td>1</td>
    </tr>
    <tr>
      <th>25</th>
      <td>12</td>
      <td>2</td>
    </tr>
    <tr>
      <th>26</th>
      <td>12</td>
      <td>3</td>
    </tr>
    <tr>
      <th>27</th>
      <td>12</td>
      <td>6</td>
    </tr>
    <tr>
      <th>28</th>
      <td>13</td>
      <td>1</td>
    </tr>
    <tr>
      <th>29</th>
      <td>13</td>
      <td>13</td>
    </tr>
    <tr>
      <th>30</th>
      <td>14</td>
      <td>1</td>
    </tr>
    <tr>
      <th>31</th>
      <td>14</td>
      <td>2</td>
    </tr>
    <tr>
      <th>32</th>
      <td>14</td>
      <td>7</td>
    </tr>
    <tr>
      <th>33</th>
      <td>14</td>
      <td>14</td>
    </tr>
    <tr>
      <th>34</th>
      <td>15</td>
      <td>1</td>
    </tr>
    <tr>
      <th>35</th>
      <td>15</td>
      <td>3</td>
    </tr>
    <tr>
      <th>36</th>
      <td>15</td>
      <td>5</td>
    </tr>
    <tr>
      <th>37</th>
      <td>15</td>
      <td>15</td>
    </tr>
    <tr>
      <th>38</th>
      <td>16</td>
      <td>1</td>
    </tr>
    <tr>
      <th>39</th>
      <td>16</td>
      <td>2</td>
    </tr>
    <tr>
      <th>40</th>
      <td>17</td>
      <td>1</td>
    </tr>
    <tr>
      <th>41</th>
      <td>17</td>
      <td>17</td>
    </tr>
    <tr>
      <th>42</th>
      <td>18</td>
      <td>1</td>
    </tr>
    <tr>
      <th>43</th>
      <td>18</td>
      <td>2</td>
    </tr>
    <tr>
      <th>44</th>
      <td>18</td>
      <td>3</td>
    </tr>
    <tr>
      <th>45</th>
      <td>18</td>
      <td>6</td>
    </tr>
    <tr>
      <th>46</th>
      <td>19</td>
      <td>1</td>
    </tr>
    <tr>
      <th>47</th>
      <td>19</td>
      <td>19</td>
    </tr>
    <tr>
      <th>48</th>
      <td>20</td>
      <td>1</td>
    </tr>
    <tr>
      <th>49</th>
      <td>20</td>
      <td>2</td>
    </tr>
    <tr>
      <th>50</th>
      <td>20</td>
      <td>5</td>
    </tr>
    <tr>
      <th>51</th>
      <td>20</td>
      <td>10</td>
    </tr>
  </tbody>
</table>
</div>




```python
len(prime_factor_grid.columns)
```




    8




```python
with open('colors.txt','r') as f:
    colors = [ line.strip() for line in f.readlines() ]

prime_colors = { prime: colors[i%len(colors)] for i,prime in enumerate(prime_factor_grid.columns) }
prime_colors
```




    {2: '#a2b9bc',
     3: '#b2ad7f',
     5: '#878f99',
     7: '#6b5b95',
     11: '#6b5b95',
     13: '#feb236',
     17: '#d64161',
     19: '#ff7b25'}




```python
from graphviz import Digraph

dot = Digraph(comment='Divisors')
node = dot.node
edge = dot.edge

for n, prime, power in prime_factor_triples.values:
    base = n // prime
    edge(str(base), str(n), color=prime_colors[prime])

dot
```




    
![svg](README_files/README_24_0.svg)
    




```python
name, xtn = splitext(graph)
assert xtn.startswith('.')
xtn = xtn[1:]
dot.render(filename=name, format=xtn)
remove(name)  # graphviz also writes the graph in DOT format to an extension-less file, for some reason
```

## Exit w/ success msg


```python
num_primes = is_prime.sum(); num_primes
```




    8




```python
from gsmo import OK
OK(f'Found {num_primes} primes ∈ [1,{limit})', throw=False)
```




    gsmo.control.OK('Found 8 primes ∈ [1,20)')


